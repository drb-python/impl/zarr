from drb.exceptions.core import DrbException


class DrbZarrNodeException(DrbException):
    pass
