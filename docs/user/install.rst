.. _install:

Installation of zarr driver
====================================
To include this module into your project, the ``drb-driver-zarr`` module shall be referenced into requirements.txt file,
or the following pip line can be run:

.. code-block::

    pip install drb-driver-zarr
