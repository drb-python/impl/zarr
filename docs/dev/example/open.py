import zarr.core
from drb.drivers.file import DrbFileFactory


from drb.drivers.zarr import DrbZarrFactory

path_file = "path/to_your/zarr.zarr"

node_file = DrbFileFactory().create(path_file)
node = DrbZarrFactory().create(node_file)

# Get node name
node.name

# Get the full path of the node
node.path.original_path

# Get the DrbZarrGroupNode
node["name_group_node"]

# Get the DrbZarrArrayNode
node["name_group_node"]["name_array_node"]

# Get the arrray of values
impl = node["name_group_node"]["name_array_node"].get_impl(zarr.core.Array)

# Get an attribute corresponding to a name.
attr = node["name_group_node"] @ "myattr"

# Get all the attributes of a node.
dict_attr = node["name_group_node"].attribute_name()
