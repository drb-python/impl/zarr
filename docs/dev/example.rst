.. _example:

Examples
=========

Open and read a zarr file
-----------------------------
.. literalinclude:: example/open.py
    :language: python
