===================
Data Request Broker
===================
---------------------------------
ZARR driver for DRB
---------------------------------
Release v\ |version|.


.. image:: https://pepy.tech/badge/drb-driver-zarr/month
    :target: https://pepy.tech/project/drb-driver-zarr
    :alt: Requests Downloads Per Month Badge

.. image:: https://img.shields.io/pypi/l/drb-driver-zarr.svg
    :target: https://pypi.org/project/drb-driver-zarr/
    :alt: License Badge

.. image:: https://img.shields.io/pypi/wheel/drb-driver-zarr.svg
    :target: https://pypi.org/project/drb-driver-zarr/
    :alt: Wheel Support Badge

.. image:: https://img.shields.io/pypi/pyversions/drb-driver-zarr.svg
    :target: https://pypi.org/project/drb-driver-zarr/
    :alt: Python Version Support Badge

-------------------

This drb-driver-zarr module implements access to zarr containers with DRB data model.
It is able to navigates among the zarr contents.

User Guide
==========

.. toctree::
   :maxdepth: 2

   user/install


.. toctree::

   dev/api

Example
=======

.. toctree::
   :maxdepth: 2

   dev/example

